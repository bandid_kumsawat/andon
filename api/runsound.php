<?php
    // shell_exec("aplay /home/pi/sound/sound/".$_GET["sound_name"]);
    // $arr_put = array("status"=>1);
    // echo (json_encode($arr_put));

    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_PORT => "1880",
    CURLOPT_URL => "http://localhost:1880/aplay?play=".$_GET['sound_name'],
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => "",
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        // echo "cURL Error #:" . $err;
        $arr_put = array("status"=>0);
        echo (json_encode($arr_put));
    } else {
        echo $response;
        $arr_put = array("status"=>1);
        echo (json_encode($arr_put));
    }
?>