import pyaudio
import wave
import time
import sys
#define stream chunk
chunk = 1024
file_name = str(sys.argv[1])
# print 'Play Sound:'+file_name+"\r\n"


def play_sound(sound_name):

    try:
        #open a wav format music
        f = wave.open(sound_name,"rb")
        #instantiate PyAudio
        p = pyaudio.PyAudio()
        #open stream
        stream = p.open(format = p.get_format_from_width(f.getsampwidth()),
                        channels = f.getnchannels(),
                        rate = f.getframerate(),
                        output = True)
        #read data
        data = f.readframes(chunk)
        #play stream
        while data:
            stream.write(data)
            data = f.readframes(chunk)
        #stop stream
        stream.stop_stream()
        stream.close()
        #close PyAudio
        p.terminate()

    except Exception as e:
        return e


play_sound(file_name)
