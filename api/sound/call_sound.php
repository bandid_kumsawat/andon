<?php
    include("config.php");
    $sql = "SELECT * from sound ORDER BY name ASC;";
    $ret = $db->query($sql);
    $arr = array();
    $i=0;
    while( $row = $ret->fetchArray(SQLITE3_ASSOC) ) {
        $arr[$i] = array(
            "id"=>$row['id'],
            "name"=>$row['name']
        );
        $i++;
    }
    $db->close();
    $arr_put = array("Total"=>$i,"List"=>$arr);
    echo (json_encode($arr_put));
?>