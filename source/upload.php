<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Upload_backend</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <?php
    // print_r(scandir("./../database"));

    // remove file .csv

    $target_dir = "../database/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // echo $target_file;
    // unlink("../database/". $_FILES["fileToUpload"]["name"]);

    // if (file_exists($target_file)) {
    //     echo "Sorry, file already exists.";
    //     $uploadOk = 0;
    // }
    // // Allow certain file formats
    if($imageFileType != "csv" ) {
        echo "Sorry, only CSV.";
        $uploadOk = 0;
    }
    // // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "<br>Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
            $f = fopen("../database/main_file.txt", "w");
            $data ="".$_FILES["fileToUpload"]["name"];
            fwrite($f, $data);
            fclose($f);
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }


   //------------------------------------------------------- database -----------------------------------------------

  if ($uploadOk != 0){
    $i=0;
    $s=0;
    $arraycsv=array();
    //echo "<html><body><table>\n\n";
    $f = fopen("../database/". $_FILES["fileToUpload"]["name"], "r");
    while (($line = fgetcsv($f)) !== false) {
      //echo "<tr>";
      foreach ($line as $cell) {
        //echo "<td>" . htmlspecialchars($cell) . "</td>";
      }
      //echo "</tr>\n";
      $arraycsv[$i]=$line;
      $i++;
    }
    //echo "\n</table></body></html>";
    fclose($f);
    // echo (json_encode($arraycsv)) ;
    // print_r($arraycsv);
  }
  //----------------------------------------------------- UPDATE Data -----------------------------------------------

  class MyDB extends SQLite3 {
    function __construct() {
      $this->open('../database/Time_table.db');
    }
  }

  $db = new MyDB();
  if(!$db){
    echo $db->lastErrorMsg();
  } else {
    //echo "Opened database successfully\n";
  }
    #---------------------------------------------- delete -------------------------------------------------
  if ($uploadOk != 0){
    $sql_del = 'delete from Shipping';
    $ret = $db->exec($sql_del);
    if(!$ret) {
      echo $db->lastErrorMsg();
    } else {
      //echo "Delate successfully\n";
    }

    #---------------------------------------------- Insert -------------------------------------------------
    for($s=7;$s<$i;$s++){
      $sql_add ='INSERT INTO Shipping ( No,โตโยต้า, Route, Truck, Shippinglane, 
                                        Watingpost_in, Watingpost_out, Shipping_input,Shipping_out, Prepare_input, 
                                        Prepare_out, Checker_input, Checker_out, WaitingLoadingtime2hrs,รอบเวลามารับ_input, 
                                        รอบเวลามารับ_out, รอบกำหนดเวลาส่ง_input, รอบกำหนดเวลาส่ง_out, ขอปรับเวลามารับ)
                 VALUES     ( "'.$s.'","'.$arraycsv[$s][1].'","'.$arraycsv[$s][2].'","'.$arraycsv[$s][3].'","'.$arraycsv[$s][4].'",
                              "'.$arraycsv[$s][5].'","'.$arraycsv[$s][6].'","'.$arraycsv[$s][7].'","'.$arraycsv[$s][8].'","'.$arraycsv[$s][9].'",
                         "'.$arraycsv[$s][10].'","'.$arraycsv[$s][11].'","'.$arraycsv[$s][12].'","","'.$arraycsv[$s][13].'","'.$arraycsv[$s][14].'",
                         "'.$arraycsv[$s][15].'","'.$arraycsv[$s][16].'","'.$arraycsv[$s][17].'")';
      // echo $sql_add;
      $ret = $db->exec($sql_add);
      if(!$ret) {
        echo $db->lastErrorMsg();
      }
    }
    //echo "Records successfully :".$s."\n";
    $db->close();
  }
  ?>
  <script>
    <?php
        if ($uploadOk != 0){
          echo 'location.href = "time_t.php";';
        }else{
          echo 'alert("ระบบไม่สามารถอัพโหลดไฟล์ได้");location.href = "time_t.php";';
        }
    ?>
  </script>
</body>
</html>