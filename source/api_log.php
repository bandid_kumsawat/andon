<?php
  $db = new SQLite3('../database/status.db');
  // On connect
  $arr = array();
  $arr_put = array();
  $i=0;
  $sql = "SELECT *
          FROM logs";
  $results = $db->query($sql);
  while ($row = $results->fetchArray()) {
    $arr[$i] = array(
                       "id"=>$row['id'],
                       "lane"=>$row['line'],
                       "color"=>$row['color'],
                       "state"=>$row['state'],
                       "mode"=>$row['mode'],
                       "time"=>$row['time']
                   );
     $i++;
  }
  $db->close();

  $arr_put = array("Total"=>$i,"List"=>$arr);
  echo (json_encode($arr_put)) ;
?>
