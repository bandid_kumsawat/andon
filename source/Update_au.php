<?php include("config.php");?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AGC</title>

  <!-- Bootstrap -->
<link href="../css/bootstrap.min.css" rel="stylesheet">



<link href='../packages/core/main.css' rel='stylesheet' />
<link href='../packages/daygrid/main.css' rel='stylesheet' />
<link href='../packages/timegrid/main.css' rel='stylesheet' />


<script src='../packages/core/main.js'></script>
<script src='../packages/interaction/main.js'></script>
<script src='../packages/daygrid/main.js'></script>
<script src='../packages/timegrid/main.js'></script>



  <style>
    body {
      /*background-color: lightblue;*/
    }
    th, td {
                    text-align: center;
                    vertical-align: center;
                }
    .btn-orange {
                  color: #FAFAFA;
                  background-color: #ff9933;
                  border-color: #A8A119;
                }
    .btn-violet {
                  color: #FAFAFA;
                  background-color: #7E4EBD;
                  border-color: #130269;
                }
    #calendar {
                max-width: 600px;
                margin: 0 auto;
                }
    .upload {
        background-color: #5bc0de;
        padding: 10px 24px;
        border-radius: 8px;
        display: inline;

        font-size: 75%;
        font-weight: 700;
        line-height: 1;
        color:#fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
    }
    .btn-pa {
        color: #fff;
        background-color: #33CC66;
        border-color: #00CC00;
    }
    .btn-pa:hover {
        color:#fff;
        background-color:#00CC00;
        border-color:#00CC00;
    }    
  </style>

  <script type="text/javascript" >

  </script>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

  <?php include("Header.php");?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12"><br><br><br><br></div>
    </div>

    <div class="container well">
        <div class="panel panel-info" style="padding: 19px;">
        <form action = "./../api/sound/sound.php" method = "POST" enctype='multipart/form-data'   >
            <div class='row'>
                <div class = 'col-md-12'>
                    <center>
                    <h1>อัพโหลดไฟล์เสียง</h1>
                    <br>
                        <h4>เลือกไฟล์
                        <input name="wav_file" type="file" class="form-control-file" style="display: unset;" accept=".wav"/>
                        <button class="btn btn-info" data-toggle="modal" data-target="#exampleModal">Upload</button></h4>
                    </center>
                </div>
            </div>
        </form>
    <br>


      <div class="row">
        <div class="col-sm-12"><center><h3><b>รายการอัพโหลด</b></h3></center></div>
      </div>

      <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="text-center default">ซื่อไฟล์</th>
                        <th class="text-center default">เล่นเสียง</th>
                        <th class="text-center default">เล่นเสียงที่lane</th>
                        <th class="text-center default">จัดการ</th>
                    </tr>
                </thead>
                <tbody class="text-center" id="date_sound">
                </tbody>
            </table>
        </div>
    </div>
</div>
  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/date.js"></script>
  <script src="../js/calender_andon.js"></script>
  <script src="../js/Moment.js"></script>
  <script src="../js/bootstrap-datepicker.js"></script>
  <script src="../js/bootstrap-datepicker.min.js"></script>
  <script src="../js/bootstrap-datetimepicker.min.js"></script>
  <script type="text/javascript">
    // var data_calender_json = JSON.parse('')
    $(document).ready(function(){
        // pot_calender_js(data_calender_json);
        date_time('date_time');
        call_sound();
    });
    
    function call_sound(){
      var settings = {
          "async": true,
          "crossDomain": true,
          "url": "../api/sound/call_sound.php",
          "method": "GET"
        }

      $.ajax(settings).done(function (response) {
        var log = JSON.parse(response);
        console.log(log);
        for(var i=0; i<log.Total; i++){
          $('#date_sound').append(
            '<tr>'+
              '<td>'+log.List[i].name+'</td>'+
              '<td>'+'<audio controls><source src="./../api/sound/sound/'+log.List[i].name+'" type="audio/ogg"></audio>'+'</td>'+
              '<td><button type="button" class="btn btn-pa" onclick = "play_sound(\''+log.List[i].name+'\')">เล่นเสียง</button></td>'+
              '<td><button type="button" class="btn btn-danger" onclick = "delete_sound(\''+log.List[i].id+'\')">ลบ</button></td>'+
            '</tr>'
          );
        }

      }); 
    }

    function delete_sound(id){
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "../api/sound/delete.php?sound_delete=" + id,
          "method": "GET"
        }

        $.ajax(settings).done(function (response) {
            var log = JSON.parse(response);
            console.log(log);
            try{
                if (log.status == 1){
                    location.reload();
                }
            }catch (e){
                location.reload();
            }
        }); 
    }

    function play_sound(name){
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "../api/sound/runsound.php?sound_name=" + name,
          "method": "GET"
        }
        console.log(settings);
        $.ajax(settings).done(function (response) {
            var log = JSON.parse(response);
            console.log(log);
        }); 
    }


  </script>

</body>
</html>
