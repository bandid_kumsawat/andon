<?php
  date_default_timezone_set("Asia/Bangkok");

  $obj = new \stdClass();

  $obj -> date = date("d");
  $obj -> month = date("n");
  $obj -> year = date("Y");
  $obj -> time = date("H:i:s");

  $myJSON =  json_encode($obj);
  echo $myJSON;

?>
