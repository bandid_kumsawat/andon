<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8' />
    <link href='../packages/core/main.css' rel='stylesheet' />
    <link href='../packages/daygrid/main.css' rel='stylesheet' />
    <link href='../packages/timegrid/main.css' rel='stylesheet' />
    <link href='../packages/list/main.css' rel='stylesheet' />
    <script src='../packages/core/main.js'></script>
    <script src='../packages/interaction/main.js'></script>
    <script src='../packages/daygrid/main.js'></script>
    <script src='../packages/timegrid/main.js'></script>
    <script src='../packages/list/main.js'></script>
    <script>


        var test_func = function (arg) {
            console.log(arg)
        }

        document.addEventListener('DOMContentLoaded', function () {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: ['interaction', 'dayGrid', 'timeGrid'],
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth'
                },
                selectable: true,
                selectMirror: true,
                // select: function (arg) {
                //     console.log(arg)
                // },
                select: test_func,
                // defaultDate: '2019-10-12',
                navLinks: true, // can click day/week names to navigate views
                businessHours: true, // display business hours
                businessHours: [ // specify an array instead
                    {
                        daysOfWeek: [1, 2, 3, 4, 5, 6], // Monday, Tuesday, Wednesday
                        // startTime: '08:00', // 8am
                        // endTime: '18:00' // 6pm
                        color: '#ff9f89'
                    }
                ],
                editable: true,
                events: [
                    {
                        title: 'Update ตารางล่วงหน้า',
                        start: '2019-09-30',
                        color: '#257e4a'
                    },
                    {
                        groupId: "CC22",    //route
                        title: 'ปรับรอบเวลา CC22',
                        start: '2019-10-15T11:00:00',
                        end: '2019-10-21T11:00:00',
                        color: '#ff9f89'
                    },
                    {
                        groupId: "CC13",
                        truck: '4',
                        title: 'ปรับรอบเวลา BanPho (P3) 1:6:X Lane 1,2',
                        start: '2019-10-07',
                        end: '2019-10-10'
                    },
                    {
                        groupId: "CC14",
                        truck: '5',
                        title: 'ปรับรอบเวลา BanPho (P3) 1:6:X ,5,Lane 3,4',
                        start: '2019-10-07',
                        end: '2019-10-10'
                    },
                    {
                        groupId: "CC15",
                        truck: '6',
                        title: 'ปรับรอบเวลา BanPho (P3) 1:6:X ,6,Lane 3,4',
                        start: '2019-10-07',
                        end: '2019-10-10',
                    },

                    // // areas where "Meeting" must be dropped

                    // {
                    //     groupId: 'availableForMeeting',
                    //     start: '2019-08-13T10:00:00',
                    //     end: '2019-08-13T16:00:00',
                    //     rendering: 'background'
                    // },

                    // // red areas where no events can be dropped

                    {
                        title: "วันหยุด",
                        start: '2019-10-14',
                        overlap: false,
                        rendering: 'background',
                        color: '#ff9f89'
                    }
                ], selectOverlap: function (event) {
                    // return event.rendering === 'background';
                    console.log(event)


                }
            });

            calendar.on('dateClick', function (info) {
                console.log('clicked on ' + info);
            });

            calendar.on('selectionInfo', function (info) {
                console.log('info  :' + info);
            });



            calendar.render();
        });

    </script>
    <style>
        body {
            margin: 40px 10px;
            padding: 0;
            font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
            font-size: 14px;
        }

        #calendar {
            max-width: 900px;
            margin: 0 auto;
        }
    </style>
</head>

<body>

    <div id='calendar'></div>

</body>

</html>