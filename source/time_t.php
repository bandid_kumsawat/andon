<?php include("config.php");?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Time_Table</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/bootstrap-datepicker.min.css" >
  <link rel="stylesheet" href="../css/bootstrap-datetimepicker.min.css">
  <link href="../css/datatables.css" rel="stylesheet">
  <style type="text/css">
    .lane-for-view{
      font-weight: 600;
      font-size: 18px;
    }
  </style>

</head>
<body>

  <?php include("Header.php");?>

  <br><br>
  <div class="container-fluid">
    <br>
    <div class="row clearfix">
      <div class="col-md-12 table-responsive">
        <table class="table table-bordered table-hover table-sortable table-striped">
          <thead>
            <tr >
              <th class="text-center" colspan="15">
                <h3><B>Shipping Visual Control Table</B></h3>
              </th>
              <th class="text-center" colspan="1">
                <button type="button" class="btn btn-info btn-lg glyphicon glyphicon-menu-hamburger" id="modal"></button>
              </th>
            </tr >
          </thead>
        </table>
        <table class="table table-striped table-bordered" id="tab_logic">
          <thead>
            <tr>
              <th class="text-center success" rowspan='2'>
                TOYOTA
              </th>
              <th class="text-center info" rowspan='2'>
                Route
              </th>
              <th class="text-center danger" rowspan='2'>
                Truck
              </th>
              <th class="text-center warning" rowspan='2'>
                Shipping Lane
              </th>


              <th class="text-center success" colspan="2">
                Shopping
              </th>
              <th class="text-center success" colspan="2">
                Prepare
              </th>
              <th class="text-center success" colspan="2">
                Checker
              </th>


              <th class="text-center danger" rowspan='2'>
                WLT 2 hrs. <!-- Waiting Loading time 2 hrs. -->
              </th>


              <th class="text-center warning" colspan="2">
                รอบเวลารับ
              </th>
              <th class="text-center warning" colspan="2">
                รอบเวลาส่ง
              </th>


              <th class="text-center danger" rowspan='2'>
                ปรับเวลารับ
              </th>
            </tr>
            <tr>
              <!-- <th></th>
              <th></th>
              <th></th>
              <th></th> -->

              <!-- <th>Shopping</th> -->
              <th>in</th>
              <th>out</th>


              <!-- <th>Prepare</th> -->
              <th>in</th>
              <th>out</th>

              <!-- checker -->
              <th>in</th>
              <th>out</th>

              <!-- <th>WLT 2 hrs.</th> -->

              <!-- <th>รอบเวลารับ</th> -->
              <th>in</th>
              <th>out</th>

              <!-- <th>รอบเวลาส่ง</th> -->
              <th>in</th>
              <th>out</th>

              <!-- <th>ปรับเวลารับ</th> -->
            </tr>
          </thead>

          <tbody class="text-center">
              <?php
                $sql = "SELECT * from Shipping";
                $ret = $db->query($sql);
                while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
                    ?>
                    <tr>
                      <td><?php echo $row['โตโยต้า']; ?></td>
                      <td><?php echo $row['Route']; ?></td>
                      <td><?php echo $row['Truck']; ?></td>
                      <td><?php echo $row['Shippinglane']; ?></td>

                      <td><?php echo $row['Shipping_input']; ?></td>
                      <td><?php echo $row['Shipping_out']; ?></td>

                      <td><?php echo $row['Prepare_input']; ?></td>
                      <td><?php echo $row['Prepare_out']; ?></td>

                      <td><?php echo $row['Checker_input']; ?></td>
                      <td><?php echo $row['Checker_out']; ?></td>

                      <td><?php echo $row['WaitingLoadingtime2hrs']; ?></td>


                      <td><?php echo $row['รอบเวลามารับ_input']; ?></td>
                      <td><?php echo $row['รอบเวลามารับ_out']; ?></td>


                      <td><?php echo $row['รอบกำหนดเวลาส่ง_input']; ?></td>
                      <td><?php echo $row['รอบกำหนดเวลาส่ง_out']; ?></td>
                      
                      <td><?php echo $row['ขอปรับเวลามารับ']; ?></td>
                    </tr>
              <?php
                 }
              ?>
          </tbody>
        </table>
      </div>
    </div>
    <!-- <a id="add_row" class="btn btn-default pull-right">Add Row</a> -->
  </div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Menu</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid">

            <div class="row">
              <div class="col-md-12">
                <h5> Upload File .CSV </h5>
                <br>
                <form class="form-inline" action="upload.php" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <input type="file" name="fileToUpload" id="fileToUpload" class="btn btn-info btn-md">
                  </div>
                  <input type="submit" value="Upload File" name="submit" class="btn btn-info btn-md">
                  <?php 
                    $f = fopen("../database/main_file.txt", "r");
                    $data = fread($f,filesize("../database/main_file.txt"));
                    fclose($f);
                  ?>
                  <a class = "btn btn-success" href = "./../database/<?php echo $data;?>">Download .csv</a>
                </form>
                <hr>
              </div>
            </div>

            <div clas="row">
              <div class="col-md-12">
                <h5> SET MODE </h5>

                  <button  type="button" class="glyphicon glyphicon-wrench btn btn-info btn-block" data-toggle="modal" data-target="#largeShoes"></button>
                  
                <hr>
                <br>
              </div>
            </div>

            <div clas="row">
              <div class="col-md-12">
                <h5> Select Holiday </h5>
                <br>
                <form action="set_holiday.php" method="post">
                  <!-- <input type="text"  value="" name="holiday_s" id="sandbox">
                  <span >
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span> -->

                  <div class="input-group date" >
                      <input type="text" name="holiday_s" class="form-control" id="sandbox">
                      <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                      </div>
                  </div>
                  <br>
                  <button type="submit" class="glyphicon glyphicon-th btn btn-info btn-block" id="calendar"></button>
                </form>

                <hr>
                <br>
              </div>
            </div>

            <div clas="row">
              <div class="col-md-12">
                <h5> Set date in Pi </h5>
                <br>
                <form action="setdate.php" method="get">

                  <div class='input-group date ' id='setdatetime'>
                      <input type='text' class="form-control" name="settime" id="settime" value="">
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-cog"></span>
                      </span>
                  </div>
                  
                  <br>
                  <button type="submit" class="glyphicon glyphicon-cog btn btn-info btn-block" id="time"></button>
                </form>

                <hr>
                <br>
              </div>
            </div>


          </div>
        </div>
      </div>
    </div>
  </div>

<!-- The modal -->
<div class="modal fade" id="largeShoes" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="modalLabelLarge">Set Mode</h4>
      </div>

      <div class="modal-body">
        <div class="row" id = "lane_a_and_m">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
  <script src="../js/jquery-3.3.1.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/Moment.js"></script>
  <script src="../js/bootstrap-datepicker.js"></script>
  <script src="../js/bootstrap-datepicker.min.js"></script>
  <script src="../js/bootstrap-datetimepicker.min.js"></script>
  <script src="../js/date.js"></script>
  <script src="../js/datatables.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      status();
      
      date_time('date_time');
      $('#sandbox').datepicker({
        format: "dd/mm/yy",
        multidate: true,
        daysOfWeekHighlighted: "0",
        todayHighlight: false
      });

      $('#setdatetime').datetimepicker({
        format:'YYYY-MM-DD HH:mm:ss'
      });

      $("#calendar").click(function(){
        var print = $('#sandbox').val();
        console.log(print);
      });

      $("#time").click(function(){
        var print_settime = $('#settime').val();
        console.log(print_settime);
      });

      $('#modal').on('click' , function(){
        $('#myModal').modal('show');
      });
      var table_d = $("#tab_logic").DataTable()
    });
    function set_mode(lane,mode){
      console.log($("#lane_a_and_m").val() + " " + mode);
      var form = new FormData();
      form.append("lane", lane);
      form.append("mode", mode);
      console.log(form);
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "set_mode.php",
        "method": "POST",
        "headers": {
          "Accept": "*/*",
          "Cache-Control": "no-cache",
          "cache-control": "no-cache"
        },
        "processData": false,
        "contentType": false,
        // "data": "{'lane':"+$("#lane_a_and_m").val()+",'mode':"+mode+"}"
        "data" : form
      }
      console.log(settings);
      $.ajax(settings).done(function (response) {
        console.log(response)
        var obj = JSON.parse(response);
        if (obj.status == 1){
          status();
        }
      });
    }
    function status(){
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "api_status.php",
        "method": "GET"
      }

      $.ajax(settings).done(function (response) {
        //console.log(JSON.parse(response));
        var st = JSON.parse(response);
        $("#lane_a_and_m").html("");
        for(var i=0; i<st.Total; i++){
          //console.log(st.List[i].status);   
          $('#lane_a_and_m').append(
            "<div class='col-md-6 lane-for-view'>Lane "+st.m_and_a[i].lane_a_m+' [ '+((st.m_and_a[i].select_a_and_m == 'a') ? "<span style = 'color:#129bec;'>Auto</span>" : "<span style = 'color:#e14cec;'>Manual</span>")+" ]</div>"+
            "<div class='col-md-3'>"+
            "<button type='button' style= 'background-color:#bde2f8' class='btn  btn-block' onclick = \"set_mode("+st.m_and_a[i].lane_a_m+",'a')\"> AUTO </button>"+
            "</div>"+
            "<div class='col-md-3'>"+
            "<button type='button' style= 'background-color:#f4bdf8' class='btn  btn-block' onclick = \"set_mode("+st.m_and_a[i].lane_a_m+",'m')\"> MANUAL </button>"+
            "</div>"+
            "<br><hr><br>"
            
          );
        }
      });
    }


  </script>
</body>
</html>
