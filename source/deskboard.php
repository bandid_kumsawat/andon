<?php include("config.php");?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AGC</title>

  <!-- Bootstrap -->
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <style>
    body {
      //background-color: lightblue;
    }
    th, td {
      text-align: center;
      vertical-align: center;

    }
    .btn-orange {
                  color: #FAFAFA;
                  background-color: #ff9933;
                  border-color: #A8A119;
                }
    .btn-violet {
                  color: #FAFAFA;
                  background-color: #7E4EBD;
                  border-color: #130269;
                }
  </style>

  <script type="text/javascript" >

  </script>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

  <?php include("Header.php");?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12"><br></br><br></br></div>
    </div>
  </div>
  <table class="table" style="width:100%">
    <thead id="btn">

    </thead>
  </table>
  <div class="container well">

    <div class="col-md-12 panel panel-info">
      <div class="panel-body ">
        <div class="col-md-12">
          <center><h2><span class="label label-info" id='label_lane'>LANE <?php if(isset($_POST['lane_select'])) echo $_POST['lane_select'];?></span></h2></center>
          <br>
        </div>

        <div class="col-md-12">
          <div class="Table-div">
            <table class="table table-bordered table-hover">
              <thead>
                <tr >
                  <th class="text-center success">
                    TOYOTA
                  </th>
                  <th class="text-center info">
                    Route
                  </th>
                  <th class="text-center danger">
                    Truck
                  </th>
                  <th class="text-center warning">
                    Shipping Lane
                  </th>
                  <th class="text-center success" colspan="2">
                    Shopping
                  </th>
                  <th class="text-center success" colspan="2">
                    Prepare
                  </th>
                  <th class="text-center success" colspan="2">
                    Checker
                  </th>
                  <!-- <th class="text-center danger">
                    WLT 2 hrs.  Waiting Loading time 2 hrs.
                  </th> -->
                  <th class="text-center warning" colspan="2">
                    รอบเวลารับ
                  </th>
                  <th class="text-center warning" colspan="2">
                    รอบเวลาส่ง
                  </th>
                  <th class="text-center warning">
                    ขอปรับเวลารับ
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php
                    if(isset($_POST['lane_select'])) {
                      $sql_line = $_POST['lane_select'];
                      $sql = 'SELECT * FROM "Shipping" WHERE 1';

                      $result_api = $db->query($sql);
                      while ($row = $result_api->fetchArray(SQLITE3_ASSOC)){
                        ?>
                        <tr>
                          <?php
                          for ($i = 0;$i < count(explode(",",$row['Shippinglane']));$i++){
                            if($_POST['lane_select'] == explode(",",$row['Shippinglane'])[$i]){
                          ?>
                            <td><?php echo $row['โตโยต้า']; ?></td>
                            <td><?php echo $row['Route']; ?></td>
                            <td><?php echo $row['Truck']; ?></td>
                            <td><?php echo explode(",",$row['Shippinglane'])[$i]; ?></td>
                            <td><?php echo $row['Shipping_input']; ?></td>
                            <td><?php echo $row['Shipping_out']; ?></td>
                            <td><?php echo $row['Prepare_input']; ?></td>
                            <td><?php echo $row['Prepare_out']; ?></td>
                            <td><?php echo $row['Checker_input']; ?></td>
                            <td><?php echo $row['Checker_out']; ?></td>
                            <!-- <td><?php //echo $row['WaitingLoadingtime2hrs']; ?></td> -->
                            <td><?php echo $row['รอบเวลามารับ_input']; ?></td>
                            <td><?php echo $row['รอบเวลามารับ_out']; ?></td>
                            <td><?php echo $row['รอบกำหนดเวลาส่ง_input']; ?></td>
                            <td><?php echo $row['รอบกำหนดเวลาส่ง_out']; ?></td>
                            <td><?php echo $row['ขอปรับเวลามารับ']; ?></td>
                            <?php 
                            }
                          }
                            ?>
                      </tr>
                      <?php
                      }
                    }else{
                      $sql_line = 1;
                      $sql = 'SELECT * FROM "Shipping" WHERE 1';

                      $result_api = $db->query($sql);
                      while ($row = $result_api->fetchArray(SQLITE3_ASSOC)){
                        ?>
                        <tr>
                          <?php
                          for ($i = 0;$i < count(explode(",",$row['Shippinglane']));$i++){
                            if(1 == explode(",",$row['Shippinglane'])[$i]){
                          ?>
                            <td><?php echo $row['โตโยต้า']; ?></td>
                            <td><?php echo $row['Route']; ?></td>
                            <td><?php echo $row['Truck']; ?></td>
                            <td><?php echo explode(",",$row['Shippinglane'])[$i]; ?></td>
                            <td><?php echo $row['Shipping_input']; ?></td>
                            <td><?php echo $row['Shipping_out']; ?></td>
                            <td><?php echo $row['Prepare_input']; ?></td>
                            <td><?php echo $row['Prepare_out']; ?></td>
                            <td><?php echo $row['Checker_input']; ?></td>
                            <td><?php echo $row['Checker_out']; ?></td>
                            <!-- <td><?php //echo $row['WaitingLoadingtime2hrs']; ?></td> -->
                            <td><?php echo $row['รอบเวลามารับ_input']; ?></td>
                            <td><?php echo $row['รอบเวลามารับ_out']; ?></td>
                            <td><?php echo $row['รอบกำหนดเวลาส่ง_input']; ?></td>
                            <td><?php echo $row['รอบกำหนดเวลาส่ง_out']; ?></td>
                            <td><?php echo $row['ขอปรับเวลามารับ']; ?></td>
                            <?php 
                            }
                          }
                            ?>
                      </tr>
                      <?php
                      }
                    }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="settingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <span id="connectionStatus">ADD DATA LANE</span>
            </div>
            <div class="panel-body" id="serverCollapse">

              <form class="form-horizontal" action="" method="post" >

                <div class="form-group">
                  <div class="control-label col-md-12">
                    <div class="col-md-2">
                      <label>TIME :</label>
                    </div>
                    <div class="col-md-6">
                      <input type="text" class="form-control" id="time" name="time_a" value="">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="control-label col-md-12">
                    <div class="col-md-2">
                      <label>OWNER :</label>
                    </div>
                    <div class="col-md-8">
                      <input type="text" class="form-control" id="owner" name="owner_a" value="">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="control-label col-md-12">
                    <div class="col-md-2">
                      <label>DETAIL :</label>
                    </div>
                    <div class="col-md-10">
                      <input type="text" class="form-control" id="detail" name="detail_a" value="">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="control-label col-md-12">
                    <div class="col-md-2">
                      <label>LANE :</label>
                    </div>
                    <div class="col-md-2">
                      <input type="text" class="form-control" id="Lane" name="Lane_a" value="" >
                    </div>
                    <div class="control-label col-md-8">
                      <!--<a href="#" class="btn btn-info" role="button">Link Button</a>-->
                      <button type="submit" class="btn btn-info" id="submit_data">Submit</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/date.js"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      status();
      setInterval(function(){ status();  }, 5*1000);
      date_time('date_time');
    });

    function status(){
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "api_status.php",
        "method": "GET"
      }

      $.ajax(settings).done(function (response) {
        console.log(JSON.parse(response));
        var st = JSON.parse(response);
        $("#btn").html("")
        for(var i=0; i<st.Total; i++){
          console.log(st.List[i].status);
          $('#btn').append(
            '<th>'+
              '<form action = "<?php $_PHP_SELF ?>" method = "POST">'+
                '<input type = "hidden" name = "lane_select" value="'+(i+1)+'">'+
                '<button type="submit" style = "background-color:#'+check(st.List[i].status)+';border:solid #000000 1px;" class="btn" id="btn-'+(i+1)+'" >'+(i+1) + "<br>"+ ((st.m_and_a[i].select_a_and_m == 'a') ? "A" : "M") +'</button>'+
               '</form>'+ // #f4bdf8 -> M    #bde2f8 -> A
              //  style = "background-color:#'+((st.m_and_a[i].select_a_and_m == 'a') ? "bde2f8" : "f4bdf8")+';"
            '</th>'
          );
        }
      });
    }

    function check(status){
      if(status == "1"){
        return "1BCE11";
      }
      if(status == "2"){
        return "1C6CE1";
      }
      if(status == "3"){
        return "EA1717";
      }
      if(status == "4"){
        return "F7F7F7";
      }
      if(status == "5"){
        return "E9B616";
      }
      if(status == "6"){
        return "EAF00A";
      }
      if(status == "7"){
        return "E60AF0";
      }
      if(status == "0"){
        return "989598";
      }

    }

    $('#Add_1').on('click', function () {
      $('#settingModal').modal('show');
    });

    $('#settime').on('click', function(){

      // Instantiate the Shell object and invoke its execute method.
      var oShell = new ActiveXObject("Shell.Application");

      var commandtoRun = "C:\\Winnt\\Notepad.exe";
      if (inputparms != "") {
        var commandParms = document.Form1.filename.value;
      }

      // Invoke the execute method.
      oShell.ShellExecute(commandtoRun, commandParms, "", "open", "1");
    });

  </script>

</body>
<!-- <footer>
  <div class="container well">
    <div class="row">
      <p> Deaware System Co,. Ltd </p>
    </div>
  </div>
</footer> -->
</html>
