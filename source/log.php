<?php include("config.php");?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Time_Table</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/bootstrap-datepicker.min.css" >
  <link rel="stylesheet" href="../css/bootstrap-datetimepicker.min.css">
  <link rel="stylesheet" href="../css/datatables.min.css">
  <style type="text/css">
    .dd {
          border-style: solid;
          border-color: #000099;
        }
  </style>

</head>
<body>

  <?php include("Header.php");?>

  <br><br><br><br>
  <div class="container dd">
    <br>
    <div class="row clearfix">
      <div class="col-md-12 table-responsive">
        <table class="table table-bordered table-hover table-sortable table-striped" id="tab_logic">
          <thead>
            <tr >
              <!-- <th class="text-center default">
                Lane
              </th> -->
              <th class="text-center success">
                Lane
              </th>
              <th class="text-center info">
                Color
              </th>
              <th class="text-center danger">
                State
              </th>
              <th class="text-center warning">
                Mode
              </th>
              <th class="text-center success">
                time
              </th>
            </tr>
          </thead>

          <tbody class="text-center" id="data_log">

          </tbody>
        </table>
      </div>
    </div>
    <!-- <a id="add_row" class="btn btn-default pull-right">Add Row</a> -->
    <br>
  </div>

  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/Moment.js"></script>
  <script src="../js/bootstrap-datepicker.js"></script>
  <script src="../js/bootstrap-datepicker.min.js"></script>
  <script src="../js/bootstrap-datetimepicker.min.js"></script>
  <script src="../js/datatables.js"></script>
  <script src="../js/date.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      date_time('date_time');
      log();

    });

    function log(){
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "api_log.php",
        "method": "GET"
      }

      $.ajax(settings).done(function (response) {
        console.log(JSON.parse(response));
        var log = JSON.parse(response);
        for(var i=0; i<log.Total; i++){
          console.log(log.List[i].time);
          $('#data_log').append(
            '<tr>'+
              '<td>'+log.List[i].lane+'</td>'+
              '<td>'+log.List[i].color+'</td>'+
              '<td>'+log.List[i].state+'</td>'+
              '<td>'+log.List[i].mode+'</td>'+
              '<td>'+log.List[i].time+'</td>'+
            '</tr>'
          );
        }

    var table_d = $("#tab_logic").DataTable({
      dom: 'Bfrtip',
      buttons: [
        'csv'
      ]
    })
      });
    }
// test
  </script>
</body>
</html>
