<?php
    include("config.php");
    $sql = "SELECT * from Shipping";
    $ret = $db->query($sql);
    $arr = array();
    $i=0;
    while( $row = $ret->fetchArray(SQLITE3_ASSOC) ) {
        $arr[$i] = array(
            "row0"=>$row['โตโยต้า'],
            "row1"=>$row['Route'],
            "row2"=>$row['Truck'],
            "row3"=>$row['Shippinglane'],
            "row4"=>$row['Shipping_input'],
            "row5"=>$row['Shipping_out'],
            "row6"=>$row['Prepare_input'],
            "row7"=>$row['Prepare_out'],
            "row8"=>$row['Checker_input'],
            "row9"=>$row['Checker_out'],
            "row10"=>$row['WaitingLoadingtime2hrs'],
            "row11"=>$row['รอบเวลามารับ_input'],
            "row12"=>$row['รอบเวลามารับ_out'],
            "row13"=>$row['รอบกำหนดเวลาส่ง_input'],
            "row14"=>$row['รอบกำหนดเวลาส่ง_out'],
            "row15"=>$row['ขอปรับเวลามารับ']
        );
        $i++;
    }
    $db->close();
    $arr_put = array("Total"=>$i,"List"=>$arr);
    echo (json_encode($arr_put)) ;
?>