<?php include("config.php");?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AGC</title>

  <!-- Bootstrap -->
<link href="../css/bootstrap.min.css" rel="stylesheet">



<link href='../packages/core/main.css' rel='stylesheet' />
<link href='../packages/daygrid/main.css' rel='stylesheet' />
<link href='../packages/timegrid/main.css' rel='stylesheet' />


<script src='../packages/core/main.js'></script>
<script src='../packages/interaction/main.js'></script>
<script src='../packages/daygrid/main.js'></script>
<script src='../packages/timegrid/main.js'></script>



  <style>
    body {
      /*background-color: lightblue;*/
    }
    th, td {
                    text-align: center;
                    vertical-align: center;
                }
    .btn-orange {
                  color: #FAFAFA;
                  background-color: #ff9933;
                  border-color: #A8A119;
                }
    .btn-violet {
                  color: #FAFAFA;
                  background-color: #7E4EBD;
                  border-color: #130269;
                }
    #calendar {
                max-width: 600px;
                margin: 0 auto;
                }
  </style>

  <script type="text/javascript" >

  </script>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

  <?php include("Header.php");?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12"><br><br><br><br></div>
    </div>
    <div class='row'>
        <div class = 'col-md-12'>
            <div id='calendar'></div>
        </div>
    </div>
    <br><br>
    
    <div class="container">
      <div class="row">
        <div class="col-sm-6"><center><h3><b>รายการอัพเดท</b></h3></center></div>
        <div class="col-sm-6"><center><h3><b>รายการวันหยุด</b></h3></center></div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th class="text-center default">
                  หัวข้ออัพเดทล่วงหน้า
                </th>
                <th class="text-center default">
                  วันที่
                </th>
                <th class="text-center default">
                  ดาวน์โหลดไฟล์
                </th>
                <th class="text-center default">
                  จัดการ
                </th>
              </tr>
            </thead>
            <tbody class="text-center" id="date_pre_update">
                </tbody>
          </table>
        </div>
          <div class="col-sm-6">
            <table class="table table-striped table-bordered" >
              <thead>
                <tr>
                  <th class="text-center default">
                  หัวข้อวันหยุด
                  </th>
                  <th class="text-center default">
                    วันที่
                  </th>
                  <th class="text-center default">
                    จัดการ
                  </th>
                </tr>
              </thead>
              <tbody class="text-center" id="date_holiday_sp">
                </tbody>
            </table>
          </div>
        </div>
      </div>


<!-- modal for calender -->
    <form action = "./../api/calender.php" method = "POST" enctype='multipart/form-data'>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
                  <h2 class="modal-title" id = "title_call_12"></h2>
              </div>
              <div class="modal-body" id = "is_date"> <!-- id = "is_date" -->
                <div class='row'>
                  <div class='col-sm-6'>
                    <button type = 'button' id = 'btn_update' class='btn btn-info' style = "width: 100%;" onclick = 'render_ui_select_date(1)'>อัพเดทล่วงหน้า</button>
                  </div>
                  <div class='col-sm-6'>
                    <button type = 'button' id = 'btn_holiday' class='btn btn-info' style = "width: 100%;" onclick = 'render_ui_select_date(2)'>วันหยุด</button>
                  </div>
                </div>

                <div class='row'>
                  <div class='col-sm-12' id = 'content_form'>





                    <!-- <br><hr><h4><center>อัพเดทล่วงหน้า</center></h4><hr>
                    <div class="form-group">
                      <div class='row'>
                        <div class='col-sm-3' style = 'padding-top:8px;'><label for="exampleInputEmail1">หัวข้ออัพเดทล่วงหน้า</label></div>
                        <div class='col-sm-9'>
                          <input type = "text" id="de_file" name = 'de_file' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                          <input hidden id = "mode" name = 'mode'value = "1"/>
                        </div>
                      </div>
                      <div class='row'>
                        <div class='col-sm-3' style = 'padding-top:8px;'><label for="exampleInputEmail1">เลือกไฟล์</label></div>
                        <div class='col-sm-9'>
                          <input type='file' class='form-control-file' name='file_csv' id='file_csv' accept='csv/*'/>
                        </div>
                      </div>
                      <br>
                    </div> -->




                    <!-- <br><hr><h4><center>วันหยุด</center></h4><hr>
                    <div class="form-group">
                      <div class='col-sm-3' style = 'padding-top:8px;'><label for="exampleInputEmail1">Title Holiday :</label></div>
                      <div class='col-sm-9'>
                        <input type = "text" id="de_holiday" name = 'de_holiday' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                        <input hidden id = "mode" name = 'mode'value = "2"/>
                      </div>
                      <br><br>
                    </div> -->

                    <!-- <br><hr><h4><center>วันหยุด</center></h4><hr>
                    <div class="form-group">
                      <div class='col-sm-3' style = 'padding-top:8px;'><label for="exampleInputEmail1">รายละเอียด :</label></div>
                      <div class='col-sm-9'>
                        <input type = "text" id="de_file" name = 'de_file' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                      </div>
                      <br> -->
                      <!-- <div class='col-sm-2' style = 'padding-top:8px;'><label for="exampleInputEmail1">เลือกวัน :</label></div> -->
                      <!-- <div class='col-sm-10'>
                      <div class='input-group date ' id='setdatetime'>
                          <input type='text' class="form-control" name="settime" id="settime" value="">
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-cog"></span>
                          </span>
                      </div>
                    </div> -->

                      <!-- <br>
                      <br>
                      <div class='col-sm-8'></div>
                      <div class='col-sm-4' style = "margin-left: 400px;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> ปิด </button>
                        <button type="submit" class="btn btn-primary" id = ''>เพิ่มรายการ</button>
                      </div>
                    </div> -->




                  </div>
                  <input hidden id = "start_date" name= 'start_date'value = ""/>
                  <input hidden id = "end_date" name = 'end_date'value = ""/>

                  <div class='col-sm-7'></div>
                  <div class='col-sm-5' style = "margin-left: 375px;">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div>

                  
                </div>
              </div>

                <!-- <div class="form-group">
                  <label for="exampleInputEmail1">Detail file</label>
                  <input type = "text" id="de_file" name = 'de_file' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                  <input hidden id = "start_date" name= 'start_date'value = ""/>
                  <input hidden id = "end_date" name = 'end_date'value = ""/>
                </div> -->
              
                <!-- <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" id = ''>Save changes</button>
                </div> -->

              </div>
            </div>




        </div>
        </div>
    </form>



          
      <form action = "./../api/api_delete_even.php" method = "POST" enctype='multipart/form-data'>
        <div class="modal fade" id="modal_comf" tabindex="-1" role="dialog" aria-labelledby="modal_comf__" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title" id="modal_comf__">ยืนยันการทำรายการ</h4>
              </div>
              <div class="modal-body"> <!-- id = "is_date" -->
                <div class='row' id = 'content_comf'>

                  <div class='col-sm-6'>
                    <input hidden name = 'start_date' id = 'start_date_comf' value = '' />
                    <button type = 'submit' id = 'btn_comf' class='btn btn-danger' name = 'btn_comf' style = "width: 100%;">ต้องการลบ</button>
                  </div>
                  <div class='col-sm-6'>
                    <button type = 'button' data-dismiss="modal"  class='btn btn-default' style = "width: 100%;">ออก</button>
                  </div>

                </div>
              </div>
              </div>
            </div>



  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/date.js"></script>
  <script src="../js/calender_andon.js"></script>
  <script src="../js/Moment.js"></script>
  <script src="../js/bootstrap-datepicker.js"></script>
  <script src="../js/bootstrap-datepicker.min.js"></script>
  <script src="../js/bootstrap-datetimepicker.min.js"></script>
  <script type="text/javascript">
    // var data_calender_json = JSON.parse('')
    $(document).ready(function(){
        // pot_calender_js(data_calender_json);
        date_time('date_time');
        render_ui_select_date(1)
        holiday_sp();
        pre_update();
    });
    function confirm_click(date,m){
      $('#modal_comf').modal({
        show: 'ture'
      }); 
      var content = "";
      content += "<div class='col-sm-6'>"
        content += "<input hidden name = 'start_date' id = 'start_date_comf' value = '"+date+"' />"
        content += "<button type = 'submit' id = 'btn_comf' class='btn btn-danger' name = 'select_d' value = '"+m+"' style = 'width: 100%;'>ต้องการลบ</button>"
        content += "</div>"
        content += "<div class='col-sm-6'>"
        content += "<button type = 'button' id = '' data-dismiss='modal' class='btn btn-default' style = 'width: 100%;'>ออก</button>"
      content += "</div>"
      $("#content_comf").html("");
      $('#content_comf').append(content);
    }
    function pre_update(){
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "./event_data.json",
        "method": "GET"
      }

      $.ajax(settings).done(function (response) {
        console.log(response);
        var log = (response);
        for(var i=0; i<log.pre_update.length; i++){
          var full_start = full_date(
            parseInt(moment(log.pre_update[i].start,'YYYY-MM-DDTHH:mm:ss').format('YYYY')) + 543,
            moment(log.pre_update[i].start,'YYYY-MM-DDTHH:mm:ss').format('MM'), 
            moment(log.pre_update[i].start,'YYYY-MM-DDTHH:mm:ss').format('DD'), 
            moment(log.pre_update[i].start,'YYYY-MM-DDTHH:mm:ss').format('dd')
          );
          $('#date_pre_update').append(
            '<tr>'+
              '<td>'+log.pre_update[i].title+'</td>'+
              '<td>'+full_start+'</td>'+
              '<td><a href = "./../api/'+log.pre_update[i].filename+'">'+log.pre_update[i].filename+'</a></td>'+
              '<td><button type="button" class="btn btn-danger" onclick = \'confirm_click(\"'+response.pre_update[i].start+'\",1)\'>ลบ</button></td>'+
            '</tr>'
          );
        }

      }); 
    }

    function holiday_sp(){
      var settings = {
          "async": true,
          "crossDomain": true,
          "url": "./event_data.json",
          "method": "GET"
        }

      $.ajax(settings).done(function (response) {
        console.log(response);
        pot_calender_js(response);
        var log = (response);
        for(var i=0; i<log.holiday_sp.length; i++){
          var full_start = full_date(
            parseInt(moment(log.holiday_sp[i].start,'YYYY-MM-DDTHH:mm:ss').format('YYYY')) + 543,
            moment(log.holiday_sp[i].start,'YYYY-MM-DDTHH:mm:ss').format('MM'), 
            moment(log.holiday_sp[i].start,'YYYY-MM-DDTHH:mm:ss').format('DD'), 
            moment(log.holiday_sp[i].start,'YYYY-MM-DDTHH:mm:ss').format('dd')
          );
          $('#date_holiday_sp').append(
            '<tr>'+
              '<td>'+log.holiday_sp[i].title+'</td>'+
              '<td>'+full_start+'</td>'+
              '<td><button type="button" class="btn btn-danger" onclick = \'confirm_click(\"'+response.holiday_sp[i].start+'\",2)\'>ลบ</button></td>'+
            '</tr>'
          );
        }

      }); 
    }
  </script>

</body>
</html>
